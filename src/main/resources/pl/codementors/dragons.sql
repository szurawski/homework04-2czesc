CREATE USER dragons_admin IDENTIFIED by 'dr4g0ndr4g0n';
CREATE DATABASE dragons;
GRANT ALL ON dragons.* TO 'dragons_admin';

CREATE TABLE dragons (id INT NOT NULL AUTO_INCREMENT, name VARCHAR(256), color VARCHAR(256), wingspan INT, PRIMARY KEY (id));

INSERT INTO dragons (name, color, wingspan) VALUES ('Dygir', 'green', '200'), ('Bondril', 'red', '100'), ('Onosse', 'black', '250'), ('Chiri', 'yellow','50'), ('Lase', 'blue', '300');

CREATE TABLE eggs (id INT NOT NULL AUTO_INCREMENT, dragon INT, weight INT, diameter INT, PRIMARY KEY (id), FOREIGN KEY (dragon) REFERENCES dragons (id));

INSERT INTO eggs (dragon, weight, diameter) VALUES ((select id from dragons where name = 'Dygir'), 300, 20), ((select id from dragons where name = 'Dygir'), 340, 30), ((select id from dragons where name = 'Bondril'), 200, 10), ((select id from dragons where name = 'Onosse'), 230, 20), ((select id from dragons where name = 'Onosse'), 300, 25), ((select id from dragons where name = 'Onosse'), 200, 15);

CREATE VIEW dragons_eggs AS select  eggs.weight, eggs.diameter, dragons.name FROM eggs inner join dragons on eggs.dragon = dragons.id;

CREATE TABLE ornaments ( dragon_egg INT,  color VARCHAR(256), pattern VARCHAR(256), PRIMARY KEY (dragon_egg), FOREIGN KEY (dragon_egg) REFERENCES eggs (id));

INSERT INTO ornaments (dragon_egg, color, pattern) VALUES ((select id from eggs where weight = '300' and diameter = '20'), "pink", "mesh"), ((select id from eggs where weight = '340' and diameter = '30'), "black", "striped"), ((select id from eggs where weight = '200' and diameter = '10'), "yellow", "dotted"), ((select id from eggs where weight = '230' and diameter = '20'), "blue", "dotted"), ((select id from eggs where weight = '300' and diameter = '25'), "green", "striped"), ((select id from eggs where weight = '200' and diameter = '15'), "red", "mesh");

CREATE TABLE lands (id INT NOT NULL AUTO_INCREMENT, name VARCHAR(256), PRIMARY KEY (id));

CREATE TABLE posrednik (dragon_id INT, land_id INT, PRIMARY KEY (dragon_id, land_id), FOREIGN KEY (dragon_id) REFERENCES dragons (id), FOREIGN KEY (land_id) REFERENCES lands (id));

INSERT INTO lands (name) VALUES ('Froze'), ('Oswia'), ('Oscyae'), ('Ochurg');

INSERT INTO posrednik (dragon_id, land_id) VALUES ((select id from dragons where name = "Dygir"),(select id from lands where name = "Froze")),((select id from dragons where name = "Bondril"),(select id from lands where name = "Froze")), ((select id from dragons where name = "Dygir"),(select id from lands where name = "Oswia")),((select id from dragons where name = "Onosse"),(select id from lands where name = "Oswia")),((select id from dragons where name = "Chiri"),(select id from lands where name = "Oswia"));
